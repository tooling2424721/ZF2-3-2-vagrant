#!/usr/bin/env bash

sudo apt-get update;
sudo apt-get upgrade;

sudo apt-get install -y apache2=2.4.7-1ubuntu4.1;
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server=5.5.38-0ubuntu0.14.04.1;
sudo mysqladmin -u root password 'vagrant';
sudo apt-get install -y php5-mysql=5.5.9+dfsg-1ubuntu4.4;
sudo mysql_install_db;
sudo mysql_secure_installation << EOF
vagrant                         
n
n
n
n
n
n
EOF

sudo apt-get install -y php5=5.5.9+dfsg-1ubuntu4.4;
sudo apt-get install -y libapache2-mod-php5=5.5.9+dfsg-1ubuntu4.4;
sudo apt-get install -y php5-mcrypt=5.4.6-0ubuntu5;

sudo cp -R /vagrant/config/apache2/ /etc/ && sudo chown -R root:root /etc/apache2/

sudo a2enmod rewrite
sudo service apache2 restart

wget https://github.com/zendframework/ZendSkeletonApplication/archive/master.zip

sudo apt-get -y install unzip
sudo unzip master.zip -d ./master
sudo mv ./master/ZendSkeletonApplication-master ./project
sudo mv ./project /var/www/html/
rm -rf master.zip master

sudo service apache2 restart

sudo apt-get -y install language-pack-en

sudo wget https://packages.zendframework.com/releases/ZendFramework-2.3.2/ZendFramework-2.3.2.zip
sudo unzip ZendFramework-2.3.2.zip -d ./ZF2

sudo mkdir -p /usr/share/php/libzend-framework/
sudo mv ZF2/ZendFramework-2.3.2/ /usr/share/php/libzend-framework/
sudo rm -rf ZF2 ZendFramework-2.3.2.zip

sudo cp -R /vagrant/config/php.ini /etc/php5/apache2/ && sudo chown -R root:root /etc/php5/apache2/php.ini

sudo apt-get install -y debconf-utils=1.5.51ubuntu2;

# phpmyadmin

sudo apt-get install -y debconf-utils=1.5.51ubuntu2;

sudo echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | sudo debconf-set-selections
sudo echo 'phpmyadmin phpmyadmin/app-password-confirm password vagrant' | sudo debconf-set-selections
sudo echo 'phpmyadmin phpmyadmin/mysql/admin-pass password vagrant' | sudo debconf-set-selections
sudo echo 'phpmyadmin phpmyadmin/mysql/app-pass password vagrant' | sudo debconf-set-selections
sudo echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | sudo debconf-set-selections
sudo apt-get install -y phpmyadmin=4:4.0.10-1;

sudo php5enmod mcrypt
sudo service apache2 restart

# sudo cp -R /etc/apache2/ /vagrant/config/
# sudo cp -R /etc/php5/apache2/php.ini /vagrant/config/

